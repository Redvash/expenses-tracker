//Services
import firebase from 'Services/firebase.service';
import localStorageService from 'Services/localStorage.service';
import logger from 'Services/logger.service';

//Models
import CategoryModel from 'Models/category.model';

const database = firebase.database();

const saveStateToStorage = context => {
    let data = JSON.parse(JSON.stringify({
        categories: context.state.categories
    }));

    let currentUser = context.rootState.auth.currentUser;

    if (currentUser !== null) {
        return database
            .ref('/categories/' + currentUser.id)
            .set(data);
    }
    else {
        localStorageService.setKey(localStorageService.CATEGORIES_KEY, data);
        return Promise.resolve();
    }
};
const loadStateFromStorage = () => {
    let localStorageValue = localStorageService.getKey(localStorageService.CATEGORIES_KEY);
    return localStorageValue || {
        categories: []
    };
};

export default {
    name: 'categories',
    namespaced: true,
    state: {
        loadingState: false,
        categories: []
    },
    getters: {
        getCategoryById: (state) => (categoryId) => {
            return state.categories.find(category => category.id === categoryId);
        }
    },
    actions: {
        initialize({ dispatch }) {
            firebase.auth().onAuthStateChanged(user => {
                //User signed out, clear store and load initial state
                if (!user) {
                    localStorageService.deleteKey(localStorageService.CATEGORIES_KEY);
                }

                dispatch('loadCategories');
            });

            return Promise.resolve();
        },

        loadCategories(context) {
            context.commit('setLoadingState', {
                loading: true
            });

            let currentUser = context.rootState.auth.currentUser;

            if (currentUser !== null) {
                let databaseInstance = database.ref('/categories/' + currentUser.id);

                databaseInstance.once('value')
                    .then(snapshot => {
                        let loadedState = snapshot.val();

                        //New user, no previous state
                        if (!loadedState) {
                            loadedState = loadStateFromStorage();

                            //Save first version
                            saveStateToStorage(context);
                        }

                        //Initialize models in loaded state
                        loadedState.categories = loadedState.categories.map(category => new CategoryModel(category));

                        context.commit('stateLoaded', {
                            stateData: loadedState
                        });
                    })
                    .catch(error => {
                        logger.logError('Error retrieving categories state', error, true);
                    })
                    .finally(() => {
                        context.commit('setLoadingState', {
                            loading: false
                        });
                    });
            }
            else {
                context.commit('setLoadingState', {
                    loading: false
                });
                context.commit('stateLoaded', {
                    stateData: loadStateFromStorage()
                });
            }
        },

        saveCategory(context, { categoryModel }) {
            context.commit('saveCategory', { categoryModel });
            return saveStateToStorage(context);
        },
        deleteCategory(context, { categoryId }) {
            context.commit('deleteCategory', { categoryId });
            return saveStateToStorage(context);
        }
    },
    mutations: {
        setLoadingState(state, { loading }) {
            state.loadingState = loading;
        },
        stateLoaded(state, { stateData }) {
            state.categories = stateData.categories;
        },

        saveCategory(state, { categoryModel }) {
            let categoriesClone = state.categories.slice();
            let selectedCategoryIndex = state.categories.findIndex(category => category.id === categoryModel.id);

            if (selectedCategoryIndex !== -1) {
                categoriesClone[selectedCategoryIndex] = categoryModel;
            }
            else {
                categoriesClone.push(categoryModel);
            }

            state.categories = categoriesClone;
        },
        deleteCategory(state, { categoryId }) {
            let categoriesClone = state.categories.slice();
            let selectedCategoryIndex = state.categories.findIndex(category => category.id === categoryId);
            categoriesClone.splice(selectedCategoryIndex, 1);

            state.categories = categoriesClone;
        }
    }
}
