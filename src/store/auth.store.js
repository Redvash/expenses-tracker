import firebase from '../services/firebase.service'
import logger from '../services/logger.service'

import UserModel from "../models/user.model";

export default {
    name: 'auth',
    namespaced: true,
    state: {
        authenticatingUser: true,
        currentUser: null
    },
    getters: {},
    actions: {
        initialize({ commit }) {
            return new Promise((resolve) => {
                firebase.auth().onAuthStateChanged(user => {
                    commit('currentUserChanged', {
                        user: user === null ? null : new UserModel(user)
                    });
                    commit('setAuthenticatingState', {
                        authenticating: false
                    });

                    resolve();
                });
            });
        },

        authenticateUser(context) {
            context.commit('setAuthenticatingState', {
                authenticating: true
            });

            let provider = new firebase.auth.GoogleAuthProvider();

            firebase.auth().signInWithPopup(provider)
                .then(result => {
                    logger.logSuccess('You logged in successfully', result, true);
                })
                .catch(error => {
                    logger.logError('Error logging in', error, true);
                    context.commit('setAuthenticatingState', {
                        authenticating: false
                    });
                });
        },
        terminateUserSession(context) {
            context.commit('setAuthenticatingState', {
                authenticating: true
            });

            firebase.auth().signOut()
                .then(() => {
                    logger.logSuccess('You logged out successfully', null, true);
                })
                .catch(error => {
                    logger.logError('Error logging out', error, true);
                })
                .finally(() => {
                    context.commit('setAuthenticatingState', {
                        authenticating: false
                    });
                });
        }
    },
    mutations: {
        setAuthenticatingState(state, { authenticating }) {
            state.authenticatingUser = authenticating;
        },
        currentUserChanged(state, { user }) {
            state.currentUser = user;
        }
    }
};
