import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const modules = [
    require('./auth.store').default,
    require('./categories.store').default,
    require('./transactions.store').default
];

const store = new Vuex.Store();

// Register modules and wait for initialization
const initializationPromises = modules.map(module => {
    store.registerModule(module.name, module);
    return store.dispatch(module.name + '/initialize');
});

// When all modules have finished initializing, remove global app loading overlay
Promise.all(initializationPromises)
    .then(() => {
        document.getElementById('app-loading-overlay').classList.add('ready');
    });

export default store;
