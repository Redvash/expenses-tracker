//Services
import firebase from "../services/firebase.service";
import logger from "../services/logger.service";
import localStorageService from '../services/localStorage.service';

//Models
import TransactionModel from "../models/transaction.model";

const database = firebase.database();

let saveStateToStorage = context => {
    let data = JSON.parse(JSON.stringify({
        transactions: context.state.transactions
    }));

    let currentUser = context.rootState.auth.currentUser;

    if (currentUser !== null) {
        return database.ref('/transactions/' + currentUser.id).set(data);
    }
    else {
        localStorageService.setKey(localStorageService.TRANSACTIONS_KEY, data);
        return Promise.resolve();
    }
};
let loadStateFromStorage = () => {
    let localStorageValue = localStorageService.getKey(localStorageService.TRANSACTIONS_KEY);
    return localStorageValue || {
        transactions: []
    };
};

export default {
    name: 'transactions',
    namespaced: true,
    state: {
        loadingState: false,
        transactions: []
    },
    getters: {
        getTransactionById: (state) => (transactionId) => {
            return state.transactions.find(transaction => transaction.id === transactionId);
        }
    },
    actions: {
        initialize({ dispatch }) {
            firebase.auth().onAuthStateChanged(user => {
                //User signed out, clear store and load initial state
                if (!user) {
                    localStorageService.deleteKey(localStorageService.TRANSACTIONS_KEY);
                }

                dispatch('loadTransactions');
            });

            return Promise.resolve();
        },

        loadTransactions(context) {
            context.commit('setLoadingState', {
                loading: true
            });

            let currentUser = context.rootState.auth.currentUser;

            if (currentUser !== null) {
                let databaseInstance = database.ref('/transactions/' + currentUser.id);

                databaseInstance.once('value')
                    .then(snapshot => {
                        let loadedState = snapshot.val();

                        //New user, no previous state
                        if (!loadedState) {
                            loadedState = loadStateFromStorage();

                            //Save first version
                            saveStateToStorage(context);
                        }

                        //Initialize models in loaded state
                        loadedState.transactions = loadedState.transactions.map(category => new TransactionModel(category));

                        context.commit('stateLoaded', {
                            stateData: loadedState
                        });
                    })
                    .catch(error => {
                        logger.logError('Error retrieving transactions state', error, true);
                    })
                    .finally(() => {
                        context.commit('setLoadingState', {
                            loading: false
                        });
                    });
            }
            else {
                context.commit('setLoadingState', {
                    loading: false
                });
                context.commit('stateLoaded', {
                    stateData: loadStateFromStorage()
                });
            }
        },

        saveTransaction(context, { transactionModel }) {
            context.commit('saveTransaction', { transactionModel });
            saveStateToStorage(context);

            logger.logSuccess('Transaction saved successfully', transactionModel, true);
        },
        deleteTransaction(context, { transactionId }) {
            context.commit('deleteTransaction', { transactionId });
            saveStateToStorage(context);

            logger.logSuccess('Transaction deleted successfully', transactionId, true);
        }
    },
    mutations: {
        setLoadingState(state, { loading }) {
            state.loadingState = loading;
        },
        stateLoaded(state, { stateData }) {
            state.transactions = stateData.transactions;
        },

        saveTransaction(state, { transactionModel }) {
            let transactionsClone = state.transactions.slice();
            let selectedTransactionIndex = state.transactions.findIndex(transaction => transaction.id === transactionModel.id);

            if (selectedTransactionIndex === -1) {
                transactionsClone.push(transactionModel);
            }
            else {
                transactionsClone[selectedTransactionIndex] = transactionModel;
            }

            state.transactions = transactionsClone;
        },
        deleteTransaction(state, { transactionId }) {
            let selectedTransactionIndex = state.transactions.findIndex(transaction => transaction.id === transactionId);
            let transactionsClone = state.transactions.slice();
            transactionsClone.splice(selectedTransactionIndex, 1);

            state.transactions = transactionsClone;
        }
    }
}
