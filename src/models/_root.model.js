// Root model class responsible for receiving the constructor data and checking if its a model or not.
//
// In case its a model, it should simply extend is properties into the current instance, to save the
// constructor the work of having to parse all the properties again. In case the data is not a model,
// each model type constructor is responsible for parsing and mapping the data into its own properties.

export default class Root {

    constructor(data = {}) {
        // This property should be used any application logic to know if they are working with a model or not
        this._isModel = true;

        if (data._isModel) {
            Object.assign(this, data);
        }
    }
}
