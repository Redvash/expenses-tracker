import moment from 'moment'
import get from 'lodash/get'

//Config
import Enums from '../config/enums.config';

//Services
import commonService from '../services/common.service';

import RootModel from './_root.model';

export default class Transaction extends RootModel {

    constructor(data = {}) {
        super(data);

        // Only process data if it's not a model
        if (!data._isModel) {
            this.id = get(data, 'id', commonService.generateGUID());
            this.value = get(data, 'value', 0);
            this.description = get(data, 'description', '');
            this.categoryId = get(data, 'categoryId', null);
            this.date = moment(get(data, 'date', undefined));

            this.isRepeating = get(data, 'isRepeating', false);
            this.isLoan = get(data, 'isLoan', false);

            this.interval = get(data, 'interval', Enums.TransactionInterval.Monthly);
        }
    }
}
