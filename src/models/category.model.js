import get from 'lodash/get'

import commonService from 'Services/common.service';

import RootModel from 'Models/_root.model';

export default class Category extends RootModel{

    constructor(data = {}) {
        super(data);

        // Only process data if it's not a model
        if (!data._isModel) {
            this.id = get(data, 'id', commonService.generateGUID());
            this.name = get(data, 'name', '');
            this.icon = get(data, 'icon', 'fa-usd');
            this.color = get(data, 'color', '#006400');
            this.isIncome = get(data, 'isIncome', false);
        }
    }
}
