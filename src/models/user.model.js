import get from 'lodash/get'

import RootModel from './_root.model';

export default class User extends RootModel {

    constructor(data) {
        super(data);

        // Only process data if it's not a model
        if (!data._isModel) {
            this.id = get(data, 'uid', null);
            this.name = get(data, 'displayName', '');
            this.email = get(data, 'email', '');
            this.photoURL = get(data, 'photoURL', '');
        }
    }
}
