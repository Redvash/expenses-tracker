const TransactionInterval = {
    Daily: 0,
    Weekly: 1,
    Monthly: 2,
    Yearly: 3,

    properties: {
        0: {
            id: 0,
            name: 'Daily',
        },
        1: {
            id: 1,
            name: 'Weekly'
        },
        2: {
            id: 2,
            name: 'Monthly'
        },
        3: {
            id: 3,
            name: 'Yearly'
        }
    }
};

export default {
    TransactionInterval
}