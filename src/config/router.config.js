import Vue from 'vue'
import Router from 'vue-router'

import DashboardView from 'Views/dashboard.view'
import CategoriesView from 'Views/categories.view';
import TransactionsView from 'Views/transactions.view';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'dashboard',
            component: DashboardView
        },
        {
            path: '/categories',
            name: 'categories',
            component: CategoriesView
        },
        {
            path: '/transactions',
            name: 'transactions',
            component: TransactionsView
        }
    ]
})
