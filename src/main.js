import 'font-awesome/css/font-awesome.min.css'
import 'bootstrap/dist/js/bootstrap.min'
import './assets/sass/styles.scss'

import Vue from 'vue'
import App from './App.vue'
import router from './config/router.config'
import store from './store'

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
