import firebase from 'firebase'

// Initialize Firebase
let config = {
    apiKey: "AIzaSyAyWQw3GfLSMQB3Yk2N-t_1zn0ZVj2w2M8",
    authDomain: "expenses-tracker-red.firebaseapp.com",
    databaseURL: "https://expenses-tracker-red.firebaseio.com",
    projectId: "expenses-tracker-red",
    storageBucket: "expenses-tracker-red.appspot.com",
    messagingSenderId: "447232172458"
};
firebase.initializeApp(config);

export default firebase;