// Local Storage Keys
const localStorageKeys = {
    CATEGORIES_KEY: 'categories',
    TRANSACTIONS_KEY: 'transactions',
};

function getKey(key) {
    let value = localStorage.getItem(key);

    // Attempt to parse value as JSON in case it's an object
    try {
        return JSON.parse(value);
    }
    catch (error) {
        return value;
    }
}

function setKey(key, value) {
    // If value is object, stringify it
    if (typeof value === 'object') {
        value = JSON.stringify(value);
    }

    return localStorage.setItem(key, value);
}

function deleteKey(key) {
    return localStorage.removeItem(key);
}

export default {
    ...localStorageKeys,

    getKey,
    setKey,
    deleteKey
}