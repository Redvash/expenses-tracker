// This file only exists so that WebStorm knows about the alias configured in
// vue.config.js. The alias here should match the ones there.

const path = require('path');

module.exports = {
    resolve: {
        alias: {
            Components: path.resolve(__dirname, 'src/components'),
            Config: path.resolve(__dirname, 'src/config'),
            Models: path.resolve(__dirname, 'src/models'),
            Services: path.resolve(__dirname, 'src/services'),
            Store: path.resolve(__dirname, 'src/store'),
            Views: path.resolve(__dirname, 'src/views'),
        }
    }
};