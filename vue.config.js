const webpack = require('webpack');
const path = require('path');

module.exports = {
    assetsDir: undefined,
    baseUrl: undefined,
    outputDir: undefined,
    runtimeCompiler: undefined,
    productionSourceMap: undefined,
    parallel: undefined,
    css: undefined,

    configureWebpack: {
        plugins: [
            new webpack.ProvidePlugin({
                Vue: ['vue/dist/vue.esm.js', 'default'],
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                $: 'jquery',
                moment: 'moment',
            }),
        ],
        resolve: {
            alias: {
                Components: path.resolve(__dirname, 'src/components'),
                Config: path.resolve(__dirname, 'src/config'),
                Models: path.resolve(__dirname, 'src/models'),
                Services: path.resolve(__dirname, 'src/services'),
                Store: path.resolve(__dirname, 'src/store'),
                Views: path.resolve(__dirname, 'src/views'),
            }
        }
    }
};